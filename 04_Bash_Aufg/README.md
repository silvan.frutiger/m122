# Aufgabe 1:

 1.  Erzeugt Benutzer anhand einer Liste von Benutzernamen in einer
     Textdatei (via Parameter angegebenen).
     Hinweis: Benutzen Sie `useradd` und `cat`.

 2.  Fügt einen Benutzer anhand einer Liste von Gruppen in einer
     Textdatei (via Parameter angegebenen) den jeweiligen Gruppen
     hinzu.
     Hinweis: Benutzen Sie `groupadd` , `usermod` und `cat`. Achtung es gibt für jeden Benutzer jeweils eine `Initial login group` und mehrere `Supplementary groups`.

 3.  Findet alle Dateien, welche einem (via Parameter angegebenen)
     Benutzer gehören und kopiert diese an den aktuellen Ort. Die
     kopierten Dateien werden zu einem `tar.gz` Archiv zusammengefasst
     und danach gelöscht. Die Archivdatei wird mit dem Benutzernamen
     und dem aktuellen Datum benannt.
     Hinweis: Benutzen Sie `find`, `tar`, `rm` und `date`.

 4.  Ermittelt die eigene IP-Adresse und macht einen PING-Sweep für das
     Subnetz der eigenen IP. Gibt aus, welche Hosts up sind und
     speichert die IP-Adressen der Hosts in einer Textdatei.
     Hinweis: Benutzen Sie `ping` (oder `fping`), `ifconfig` und
     `grep`.

 5.  Ermittelt die Events der Stadt Zürich für das aktuellen Datum von
     usgang.ch. Erweitern Sie das Skript danach auf beliebige Städte
     (unter usgang.ch gelistete) und die Angabe eines Datums (wenn kein
     Datum angegeben wird, wird das aktuelle angewendet).
     
     Hinweis: Benutzen Sie `curl`, `grep` und `cut`. Der erste, der ein
     funktionierendes Skript für diese Aufgabe einsendet, gewinnt
     \"Gipfeli und Schoggistengeli\".



Als Informationsquelle dient folgendes Onlinebuch:

<http://openbook.rheinwerk-verlag.de/shell_programmierung/>

---
<br><br><br><br>
---

# Aufgaben 2:


 1.  Erstellen Sie einen Ordner `/root/trash` und erzeugen Sie einige
     Dateien darin. Erstellen Sie ein Skript, welches alle 5 Minuten
     die Dateien innerhalb von diesem Ordner löscht (für Infos siehe
     auch Link 3 im Anhang). Überprüfen Sie, ob ihr Skript korrekt
     eingerichtet ist, indem Sie nachsehen, ob die Files nach 5 Minuten
     gelöscht wurden.

 2.  Erstellen Sie ein Skript, mit welchem eine IP-Adressrange *bannen*
     oder *unbannen* können. Es gibt unterschiedliche Tools, womit Sie
     diese Funktionalität umsetzen können. Verwenden Sie das Internet
     zur Informationssuche.

 3.  Erstellen Sie folgende Benutzer und Gruppen. Benutzen Sie zur
     Automatisierung die Skripte aus den Übungen. Versuchen
     Sie den Prozess der Erstellung möglichst stark zu automatisieren:

     
     ![image](x_gitressourcen/u1.png)
     

 4.  Erstellen Sie folgende Ordnerstruktur und setzen Sie die
     abgebildeten Berechtigungen (Auf den Berechtigungen ist auch das
     SGID-Bit (`s`) und Sticky-Bit (`T`) abgebildet. Setzen Sie auch
     dieses. Sie finden eine Erklärung und Anleitung im zweiten Link
     zuunterst:

     
     ![image](x_gitressourcen/u2.png)
     

Als Informationsquelle dienen folgende Onlinebücher:

<http://openbook.rheinwerk-verlag.de/shell_programmierung/>

<http://linux-infopage.de/show.php?page=berichte-berechtigungen>

<http://www.zettel-it.de/docs/SUID-SGID-und-Sticky-Bit.pdf>

<https://www.howtoforge.de/anleitung/eine-kurze-einfuhrung-in-cron-jobs/>





