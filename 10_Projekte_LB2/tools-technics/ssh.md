# SSH (und SSH-Tools)

SSH ist ein Protokoll um sichere Verbindungen zwischen verschiedenen Systemen herzustellen. Es gibt fast auf jedem Betriebssytem Client wie auch Server-Software die dieses Protokoll implementieren. SSH unterstützt verschieden Authentifizierungs-Varianten, wie Passwort-Authentifizierung, Public-Private-Key-Authentifizierung und Kerberos. Hier möchte ich auf die Authentifizierung mit Public-Private-Key eingehen.

[TOC]

## Public-Private-Key-Paare auf dem Client erstellen

Als erstes muss auf dem Client ein **Public-Private-Key-Paar** erstellt werden. Dazu gibt es auf allen Unix-Umgebungen ein Tool das `sshkey-gen` heisst. Mit diesen wird ohne Parameter ein RSA-Key-Paar erzeugt und unter `$HOME/.ssh/id_rsa` und `$HOME/.ssh/id_rsa.pub` abgelegt. 
> Falls ssh in einem Skript gebraucht wird, welches in einem Cronjob läuft und wo man keine Interaktionen machen will sollte man ein Key-Paar erstellen welches nicht durch ein Passwort geschützt ist:

```
user1@client:~$ ssh-keygen -t rsa -N ""
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user1/.ssh/id_rsa): 
Your identification has been saved in /home/user1/.ssh/id_rsa.
Your public key has been saved in /home/user1/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:+ewo6HNZSEr8AukubarC8978NzlJdO8jMEvV2NceQXQ user1@client
The key's randomart image is:
+---[RSA 2048]----+
|              ooE|
|               ..|
|   o        +   o|
|  o o .  o + o o.|
| . o + .S o . ...|
|  . o o .B   .  .|
|.o   o oo O .    |
|oo+ = +  X . o   |
|=+++.=.oo + . .  |
+----[SHA256]-----+
```

Damit wurden die 2 Files `id_rsa.pub` (Public-Key) und `id_rsa` (Private-Key) im Verzeichnis `$HOME/.ssh` erstellt.

## Public-Key auf Ziel-System kopieren

Als nächstes muss man den Public-Key im File `$HOME/.ssh/authorized_keys` des Users eintragen, mit welchem man Verbindung ohne Passwort aufnehmen will. In unserem Beispiel werden wir eine Verbindung zum `user2` auf dem System mit dem Namen `server` machen. Den Eintrag kann man von Hand machen oder man benutzt das Tool `ssh-copy-id`:

```
user1@client:~$ ssh-copy-id user2@server
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/user1/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
user2@server's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'user2@server'"
and check to make sure that only the key(s) you wanted were added.
```

Und schon ist der Public-Key von `user1` im `$HOME/.ssh/authorized_keys` von `user2` auf `server` eingetragen und man kann direkt ohne Passwort von `user1` zum `user2@server` verbinden:

```
user1@client:~$ ssh user2@server
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-58-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Fri Jun 17 14:52:42 UTC 2022

  System load:  0.0               Processes:           91
  Usage of /:   2.9% of 61.80GB   Users logged in:     0
  Memory usage: 13%               IP address for eth0: 10.0.2.15
  Swap usage:   0%                IP address for eth1: 192.168.56.3

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation

273 packages can be updated.
215 updates are security updates.

user2@server:~$ 
```

Auf dem System `server` findet man im `$HOME/.ssh/authorized_keys` von `user2` nun einen Eintrag von `user1`:

```
user2@server:~$ cat .ssh/authorized_keys 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDO2R8t4RYBJXQ/xy0yzkEJQHnV6O/rJ0ERHxlRU2DglB2YorW9quBSRVQj53E88O0J7vavXKaJgvPwcKfRGZGrZl/MA4IR6yWBgTRKi3pTQi/tEQYPIVLdDamrfCsZ/OePfMydvxvI7G64WiiZJGW3eyu9HnAR5gbucmeLWDxrlTiGNvICsCukchCpSSVXcPViDCIXw2VjNCLR6w5lrnLCZg5Yg0SGFt9kyFzeMhjOV0ql43nJ2WiOpEaWHlHR+ncWBxiGIBJ2gnR99/kQrngLPQUn+vgAE/ItCKHqPzyTWbXQjsHEczMpFWrFja+NuFfhGeh0/unqJdyXxPIPgYJv user1@client
```
---

<br> <br> 

---

## SSH-Tools ohne Passwort benutzen

Von nun an kann man von `user1@client` aus auf `user2@server` ohne Passwort eine SSH-Verbindung aufbauen...

### SCP

... oder alle enderen Tools von SSH benutzen, wie zum Beispiel **scp** zum Kopieren von Files. Hier werden die files `test*` von `user1` auf System `server` kopiert und im Verzeichnis `/tmp` abgelegt:

```
user1@client:~$ scp test* user2@server:/tmp
test1                                         100%    0     0.0KB/s   00:00    
test2                                         100%    0     0.0KB/s   00:00    
test3                                         100%    0     0.0KB/s   00:00    
```

### SFTP
Man kann auch die SSH-Veriante von ftp mit dem Namen **sftp** ohne Passwort benutzen:
(Dazu braucht es keinen FTP-Server auf dem Zielsystem!)

```
user1@client:~$ sftp user2@server
Connected to server.
sftp> ls
sftp> cd /tmp
sftp> ls -ltra
drwxrwxrwt    2 root     root         4096 Jun 17 14:34 .ICE-unix
drwxrwxrwt    2 root     root         4096 Jun 17 14:34 .X11-unix
drwx------    3 root     root         4096 Jun 17 14:34 systemd-private-c0b7d8543ee249629756cef10624e11f-systemd-resolved.service-nlxVts
drwxrwxrwt    2 root     root         4096 Jun 17 14:34 .Test-unix
drwxrwxrwt    2 root     root         4096 Jun 17 14:34 .font-unix
drwxrwxrwt    2 root     root         4096 Jun 17 14:34 .XIM-unix
drwx------    2 root     root         4096 Jun 17 14:34 netplan_s7p4c18j
drwxr-xr-x   24 root     root         4096 Jun 17 14:34 ..
drwx------    2 root     root         4096 Jun 17 14:34 tmp44d69dvf
drwxr-xr-x    3 vagrant  root         4096 Jun 17 14:36 vagrant-ansible
drwxrwxrwt   11 root     root         4096 Jun 20 07:38 .
-rw-r--r--    1 user2    sus             0 Jun 20 07:39 test3
-rw-r--r--    1 user2    sus             0 Jun 20 07:39 test1
-rw-r--r--    1 user2    sus             0 Jun 20 07:39 test2
sftp> exit
```

### Commands
Oder eben direkt auf dem Zielsystem **Befehle** ausführen. Hier wird `ls -l /tmp` auf dem `server` als `user2` ausgeführt und der output mit `grep test` auf dem system client nach allen Zeilen gefiltert welche test enthalten:

```
user1@client:~$ ssh user2@server "ls -l /tmp"|grep test
-rw-r--r-- 1 user2   sus     0 Jun 20 07:39 test1
-rw-r--r-- 1 user2   sus     0 Jun 20 07:39 test2
-rw-r--r-- 1 user2   sus     0 Jun 20 07:39 test3
```
